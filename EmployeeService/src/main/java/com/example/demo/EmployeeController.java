package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService service;

	@Autowired
	private RestTemplate restTemplate;

	@RequestMapping(method = RequestMethod.GET, value = "/employee/info/{empId}")
	public EmployeeInfo getEmployeeInfo(@PathVariable String empId) {

		EmployeeInfo info = new EmployeeInfo();

		EmployeeTraining trainings = restTemplate.getForObject("http://training-service/training/" + empId,
				EmployeeTraining.class);

		info.setEmpId(empId);
		info.setName(service.findById(empId).get().getName());
		info.setTrainings(trainings.getTrainings());

		return info;

	}

	@RequestMapping("/employees")
	public List<Employee> getEmployees() {
		return service.findAll();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/employee/add")
	public void addEmployee(@RequestBody Employee emp) {
		service.addEmployee(emp);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/employee/delete/{empId}")
	public void deleteEmployee(@PathVariable String empId) {
		service.deleteEmployee(empId);
	}

	@RequestMapping("/employees/{empId}")
	public Optional<Employee> getEmployee(@PathVariable String empId) {
		return service.findById(empId);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/employee/addAll")
	public void addAll(@RequestBody List<Employee> empList) {
		service.addAll(empList);

	}

}
