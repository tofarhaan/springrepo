package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Id;

//This is a test comment

@Entity
public class Employee {

	@Id
	private String empId;
	private String name;
	private String contactNo;
	private String email;
	private String role;
	private String technology;
	private String location;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public Employee(String empId, String name, String contactNo, String email, String role, String technology,
			String location) {
		super();
		this.empId = empId;
		this.name = name;
		this.contactNo = contactNo;
		this.email = email;
		this.role = role;
		this.technology = technology;
		this.location = location;
	}

	public Employee() {

	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
