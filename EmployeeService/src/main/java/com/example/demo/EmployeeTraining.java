package com.example.demo;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EmployeeTraining {

	@Id
	private String empId;
	
	@ElementCollection
	private List<String> trainings;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public List<String> getTrainings() {
		return trainings;
	}

	public void setTrainings(List<String> trainings) {
		this.trainings = trainings;
	}

	public EmployeeTraining(String empId, List<String> trainings) {
		super();
		this.empId = empId;
		this.trainings = trainings;
	}

	public EmployeeTraining() {

	}

}
