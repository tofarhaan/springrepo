package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepo;

	public Optional<Employee> findById(String empId) {
		return employeeRepo.findById(empId);
	}

	public void addEmployee(Employee emp) {
		employeeRepo.save(emp);

	}

	public void deleteEmployee(String empId) {
		
		employeeRepo.deleteById(empId);
		
	}
	
	public void addAll(List<Employee> employeeList) {
		
		employeeRepo.saveAll(employeeList);
	}
	
	
	public List<Employee> findAll() {

		List<Employee> empList = new ArrayList<Employee>();

		for (Employee emp : employeeRepo.findAll()) {
			empList.add(emp);
		}

		return empList;
	}

}
