package com.example.demo;

import java.util.List;


public class EmployeeInfo {

	private String empId;
	private String name;
	private List<String> trainings;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getTrainings() {
		return trainings;
	}

	public void setTrainings(List<String> trainings) {
		this.trainings = trainings;
	}

	public EmployeeInfo() {

	}

	public EmployeeInfo(String empId, String name, List<String> trainings) {
		super();
		this.empId = empId;
		this.name = name;
		this.trainings = trainings;
	}

}
