package com.example.demo;

import org.springframework.data.repository.CrudRepository;

public interface TrainingRepo extends CrudRepository<EmployeeTraining, String> {

}
