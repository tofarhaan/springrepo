package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrainingController {
	

	@Autowired
	private TrainingService service;

	@RequestMapping("/trainings")
	public List<EmployeeTraining> getEmployees() {
		return service.findAll();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/training/add")
	public void addEmployee(@RequestBody EmployeeTraining emp) {
		service.addEmployee(emp);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/training/delete/{empId}")
	public void deleteEmployee(@PathVariable String empId) {
		service.deleteEmployee(empId);
	}

	@RequestMapping("/training/{empId}")
	public Optional<EmployeeTraining> getEmployee(@PathVariable String empId) {
		return service.findById(empId);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/training/addAll")
	public void addAll(@RequestBody List<EmployeeTraining> empList) {
		service.addAll(empList);
		
	}
	
	


}
