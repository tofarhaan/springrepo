package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingService {

	@Autowired
	private TrainingRepo trainingRepo;

	public Optional<EmployeeTraining> findById(String empId) {
		return trainingRepo.findById(empId);
	}

	public void addEmployee(EmployeeTraining emp) {
		trainingRepo.save(emp);

	}

	public void deleteEmployee(String empId) {
		
		trainingRepo.deleteById(empId);
		
	}
	
	public void addAll(List<EmployeeTraining> employeeList) {
		
		trainingRepo.saveAll(employeeList);
	}
	
	
	public List<EmployeeTraining> findAll() {

		List<EmployeeTraining> empList = new ArrayList<EmployeeTraining>();

		for (EmployeeTraining emp : trainingRepo.findAll()) {
			empList.add(emp);
		}

		return empList;
	}

}
